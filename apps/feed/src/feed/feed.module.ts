import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { FeedController } from './feed.controller'
import { FeedService } from './feed.service'

import { Post, PostSchema } from './schemas/post'
import { UserFeed, UserFeedSchema } from './schemas/userfeed'
import { UserFeedChunks, UserFeedChunksSchema } from './schemas/userfeedchunks'

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Post.name, schema: PostSchema },
      { name: UserFeed.name, schema: UserFeedSchema },
      { name: UserFeedChunks.name, schema: UserFeedChunksSchema }
    ])
  ],
  controllers: [FeedController],
  providers: [FeedService],
})
export class FeedModule {}
