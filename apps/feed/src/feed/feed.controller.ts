import { Controller, Logger } from '@nestjs/common'
import { MessagePattern, Payload, RpcException } from '@nestjs/microservices'
import { GET_FEED_COMMAND, IGetFeedCommandResponse } from '@app/common/clients/feed/commands/get_feed.command'
import { GET_FEED_CHUNK_COMMAND, IGetFeedChunkCommandResponse } from '@app/common/clients/feed/commands/get_feed_chunk.command'
import { FeedService } from './feed.service'

@Controller()
export class FeedController {
  constructor(
    private feedService: FeedService
  ) {}

  @MessagePattern(GET_FEED_COMMAND)
  async getFeed(@Payload() userId: string): Promise<IGetFeedCommandResponse> {
    let feed: IGetFeedCommandResponse

    try {
      feed = await this.feedService.getFeed(userId)
    } catch (error) {
      Logger.error('Unexpected error occured while getting feed', JSON.stringify(error))
      throw new RpcException(error)
    }

    return feed
  }

  @MessagePattern(GET_FEED_CHUNK_COMMAND)
  async getFeedChunk(@Payload() hash: string): Promise<IGetFeedChunkCommandResponse> {
    let feedChunk: IGetFeedChunkCommandResponse

    try {
      feedChunk = await this.feedService.getFeedChunk(hash)
    } catch (error) {
      Logger.error(`Unexpected error occured while getting feed chunk, ID=${hash}`, JSON.stringify(error))
      throw new RpcException(error)
    }

    return feedChunk
  }
}
