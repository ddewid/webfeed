import { Model } from 'mongoose'
import { Injectable, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { UserFeedEntity } from '@app/common/entities/UserFeed'
import { UserFeedChunkEntity } from '@app/common/entities/UserFeedChunk'
import { ApplicationError } from '@app/common/errors/ApplicationError'
import { EntityNotFound } from '@app/common/errors/EntityNotFound'

import { UserFeed  } from './schemas/userfeed'
import { UserFeedChunks } from './schemas/userfeedchunks'

@Injectable()
export class FeedService {
  constructor(
    @InjectModel(UserFeed.name) private userFeed: Model<UserFeed>,
    @InjectModel(UserFeedChunks.name) private userFeedChunks: Model<UserFeedChunks>
  ) {}

  async getFeed(userId: string): Promise<UserFeedEntity> {
    let userFeed: UserFeed | null

    try {
      userFeed = await this.userFeed.findOne({ userId })
    } catch (error) {
      Logger.error('Unexpected error while feed database lookup', JSON.stringify(error))
      throw new ApplicationError('Unexpected error while feed database lookup')
    }

    if (userFeed === null) {
      throw new EntityNotFound('UserFeed Not Found')
    }

    return new UserFeedEntity({
      ...userFeed.toObject()
    })
  }
  
  async getFeedChunk(chunkHash: string): Promise<UserFeedChunkEntity> {
    let userFeedChunk: UserFeedChunks | null

    try {
      userFeedChunk = await this.userFeedChunks.findOneAndUpdate({ chunkHash, visited: false }, { visited: true })
    } catch (error) {
      Logger.error('Unexpected error while feed chunk database lookup', JSON.stringify(error))
      throw new ApplicationError('Unexpected error while feed chunk database lookup')
    }

    if (userFeedChunk === null) {
      throw new EntityNotFound('UserFeedChunk Not Found')
    }

    const chunk: UserFeedChunkEntity = new UserFeedChunkEntity({
      ...userFeedChunk.toObject()
    })

    // Note @davlis
    //  In production, update should be performed in aync & event manner (EventPattern).
    //  We would send UPDATE_FEED_COMMAND and other service (write-responsible) would handle that task.
    try {
      await this.updateFeed(chunk.feedId, chunk.nextChunkHash)
    } catch (error) {
      Logger.warn('Unexpected error while updating feed', JSON.stringify(error))
      // No action on purpose
    }

    return chunk
  }

  async updateFeed(feedId: string, nextHash: string): Promise<void> {
    await this.userFeed.updateOne({ feedId }, { chunkHeadHash: nextHash })
  }
}
