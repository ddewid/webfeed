import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

//
// Note @davlis
//
//    User can create posts with text content and image.
//    In terms of {tags, likes, comments} our internal deamon services (workers) would generate/modify it in the background.
//
//    Image upload would be off-loaded to Image-service which would minimalize overhead with uploading using signed URLs.
@Schema()
export class Post extends Document {
  @Prop()
  content: string

  @Prop()
  imageUrl: string

  // TBD
  @Prop()
  tags: string[]

  // TBD
  @Prop()
  likes: any

  // TBD
  @Prop()
  comments: any

  @Prop({ index: true })
  createdBy: string

  @Prop()
  createdAt: string

  @Prop()
  updatedAt: string
}

export const PostSchema = SchemaFactory.createForClass(Post);