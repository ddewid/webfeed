import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

//
// Note @davlis
//
//    User feed would be created automatically while user creation process.
//    At the very beggining chunkHeadHash should be set to nil,
//    that would mean for system that we gonna serve some super-common content for user / or that there is no posts.
// 
//    Once user initialization finished and when user preferences are set by our internal background processing workers,
//    our deamon service would create  "Post Chunk" List (reference: 'userfeedchunks') at given moment.
// 
//    Once new Post Chunk is avaiable then "UserFeed.chunkHeadHash" would be updated by internal deamon.
//
//    We can live with index and do not worry about latency while inserting
//    as these actions are going to be performed in background
@Schema()
export class UserFeed extends Document {
  @Prop({ index: true })
  feedId: string

  // Visibility purpose only
  @Prop()
  userId: string

  // uuid
  @Prop()
  chunkHeadHash: string
}

export const UserFeedSchema = SchemaFactory.createForClass(UserFeed);