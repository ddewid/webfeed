import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

//
// Note @davlis
//
//    This entity purpose is to serve newest posts within user feed without duplicates.
//    It's data is going to be created by our internal service deamons (workers) in background.
//
//    Our Internal deamon could create userfeedchunks maximum N "chunks" at given moment.
//  
//    Once GET /feed/:chunk is called system would do async call to change head of "userfeed" (side-effect)
//
//    When provided solution with chunks on database level would not meet proper performance to application scale
//    We could then try approach with FIFO queues with hashed keys
//    Database data would be persisted only for audit purposes.
@Schema()
export class UserFeedChunks extends Document {
  @Prop({ index: true })
  feedId: string

  @Prop({ index: true })
  chunkHash: string

  @Prop()
  nextChunkHash: string | null

  // Posts at given moment
  @Prop()
  posts: any[]

  @Prop()
  visited: boolean

  @Prop()
  createdAt: Date
}

export const UserFeedChunksSchema = SchemaFactory.createForClass(UserFeedChunks);