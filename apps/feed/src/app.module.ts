import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { FeedModule } from './feed/feed.module'

const DB_URL = typeof process.env.DB_URL === 'undefined' ? 'mongodb://localhost/feed' : process.env.DB_URL

@Module({
  imports: [MongooseModule.forRoot(DB_URL), FeedModule],
})
export class ApplicationModule {}
