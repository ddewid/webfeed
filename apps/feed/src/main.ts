import { NestFactory } from '@nestjs/core'
import { Logger } from '@nestjs/common'
import { Transport, MicroserviceOptions } from '@nestjs/microservices'
import { FEED_QUEUE_NAME } from '@app/common/clients/feed/name'

import { ApplicationModule } from './app.module'

const QUEUE_URL = typeof process.env.QUEUE_URL === 'undefined' ? 'amqp://localhost:5672' : process.env.QUEUE_URL

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(ApplicationModule, {
    transport: Transport.RMQ,
    options: {
      urls: [QUEUE_URL],
      queue: FEED_QUEUE_NAME,
      queueOptions: {
        durable: false
      },
    },
  })
  app.listen(() => Logger.log('Feed microservice is listening'))
}
bootstrap()
