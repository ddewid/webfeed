import { Injectable } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { SessionEntity } from '@app/common/entities/Session'
import { GET_USER_COMMAND, IGetUserCommandResponse } from '@app/common/clients/user/commands/get_user.command'

@Injectable()
export class AuthService {
  async authorize(userServiceClient: ClientProxy, id: string): Promise<SessionEntity> {
    const response: IGetUserCommandResponse = await userServiceClient.send(GET_USER_COMMAND, id).toPromise()

    // Note: Mock example
    return new SessionEntity({
      sessionId: response.userId,
      userId: response.userId,
      expires: new Date().toDateString(),
      revoked: false
    })
  }
}
