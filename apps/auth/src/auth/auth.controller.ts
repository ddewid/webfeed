import { Controller, Inject, Logger, UseInterceptors} from '@nestjs/common'
import { MessagePattern, ClientProxy, Payload, RpcException } from '@nestjs/microservices'
import { SessionEntity } from '@app/common/entities/Session'
import { AUTHORIZE_COMMAND } from '@app/common/clients/auth/commands/authorize.command'
import { USER_SERVICE_NAME } from '@app/common/clients/user/name'
import { AuthCacheInterceptor } from './interceptors/auth.cache.interceptor'

import { AuthService } from './auth.service'

@Controller()
export class AuthController {
  constructor(
    @Inject(USER_SERVICE_NAME) private userServiceClient: ClientProxy,
    private authService: AuthService
  ) {}

  @UseInterceptors(AuthCacheInterceptor)
  @MessagePattern(AUTHORIZE_COMMAND)
  async authorize(@Payload() id: string): Promise<SessionEntity> {
    let session: SessionEntity
    try {
      session = await this.authService.authorize(this.userServiceClient, id)
    } catch (error) {
      Logger.error('Unexpected error while authorization', JSON.stringify(error))
      throw new RpcException(error)
    }

    return session
  }
}
