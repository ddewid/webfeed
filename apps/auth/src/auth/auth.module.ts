import { CacheModule, Module } from '@nestjs/common'
import { ClientsModule, Transport } from '@nestjs/microservices'
import * as redisStore from 'cache-manager-redis-store'
import { USER_SERVICE_NAME, USER_QUEUE_NAME } from '@app/common/clients/user/name'

import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'

const QUEUE_URL = typeof process.env.QUEUE_URL === 'undefined' ? 'amqp://localhost:5672' : process.env.QUEUE_URL

@Module({
  imports: [
    CacheModule.register({
      store: redisStore,
      host: 'localhost',
      port: 6379,
    }),

    ClientsModule.register([{
      name: USER_SERVICE_NAME,
      transport: Transport.RMQ,
      options: {
        urls: [QUEUE_URL],
        queue: USER_QUEUE_NAME,
        queueOptions: {
          durable: false
        },
      },
    }])
  ],
  controllers: [AuthController],
  providers: [AuthService],
})
export class AuthModule {}
