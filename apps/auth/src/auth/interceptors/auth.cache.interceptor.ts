
import { Injectable, CacheInterceptor, ExecutionContext } from '@nestjs/common'
import { RpcArgumentsHost } from '@nestjs/common/interfaces'

@Injectable()
export class AuthCacheInterceptor extends CacheInterceptor {
  trackBy(context: ExecutionContext): string {
    const ctx: RpcArgumentsHost = context.switchToRpc()
    const id: string = ctx.getData() as string

    return createAuthKey(id)
  }
}

function createAuthKey(data: string) {
  const prefix = 'userId'

  return `${prefix}:${data}`
}
