import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { UserModule } from './user/user.module'

const DB_URL = typeof process.env.DB_URL === 'undefined' ? 'mongodb://localhost/user' : process.env.DB_URL

@Module({
  imports: [MongooseModule.forRoot(DB_URL), UserModule],
})
export class ApplicationModule {}
