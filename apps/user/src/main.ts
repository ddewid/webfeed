import { NestFactory } from '@nestjs/core'
import { Logger } from '@nestjs/common'
import { Transport, MicroserviceOptions } from '@nestjs/microservices'
import { ApplicationModule } from './app.module'

const QUEUE_URL = typeof process.env.QUEUE_URL === 'undefined' ? 'amqp://localhost:5672' : process.env.QUEUE_URL
const USER_QUEUE_NAME = 'user_queue'

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(ApplicationModule, {
    transport: Transport.RMQ,
    options: {
      urls: [QUEUE_URL],
      queue: USER_QUEUE_NAME,
      queueOptions: {
        durable: false
      },
    },
  });
  app.listen(() => Logger.log('User microservice is listening'));
}
bootstrap();
