import { Model } from 'mongoose'
import { Injectable, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { UserEntity } from '@app/common/entities/User'
import { ApplicationError } from '@app/common/errors/ApplicationError'
import { EntityNotFound } from '@app/common/errors/EntityNotFound'

import { User } from './schemas/user'

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async getUser(id: string): Promise<UserEntity> {
    let user: User | null

    try {
      user = await this.userModel.findById(id)
    } catch (error) {
      Logger.error('Unexpected error while database lookup for user', JSON.stringify(error))
      throw new ApplicationError('Unexpected error while database lookup for user')
    }

    if (user === null) {
      throw new EntityNotFound('User Not Found')
    }

    return new UserEntity({
      userId: user._id,
      ...user.toObject(),
    })
  }
}
