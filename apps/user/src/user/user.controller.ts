import { Controller, Logger } from '@nestjs/common'
import { MessagePattern, Payload, RpcException } from '@nestjs/microservices'
import { GET_USER_COMMAND, IGetUserCommandResponse } from '@app/common/clients/user/commands/get_user.command'
import { UserService } from './user.service'

@Controller()
export class UserController {
  constructor(
    private readonly userService: UserService
  ) {}

  @MessagePattern(GET_USER_COMMAND)
  async getUser(@Payload() id: string): Promise<IGetUserCommandResponse> {
    let user: IGetUserCommandResponse
    try {
      user = await this.userService.getUser(id)
    } catch (error) {
      Logger.error('Unexpected error while getting user', JSON.stringify(error))
      throw new RpcException(error)
    }

    return user
  }
}
