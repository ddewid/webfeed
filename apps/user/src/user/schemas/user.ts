import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

//
// Note @davlis
//
@Schema()
export class User extends Document {
  @Prop({ index: true })
  userId: string

  @Prop({ index: true })
  email: string

  @Prop()
  name: string

  // Note: preferences
  @Prop()
  targetGroup: any

  @Prop()
  createdAt: string

  @Prop()
  updatedAt: string
}

export const UserSchema = SchemaFactory.createForClass(User);