import { Injectable, Inject, NestMiddleware, UnauthorizedException, Logger } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { Request, Response, NextFunction } from 'express'
import { get, set } from 'lodash'
import { AUTH_SERVICE_NAME } from '@app/common/clients/auth/name'
import { AUTHORIZE_COMMAND, IAuthorizeCommandResponse } from '@app/common/clients/auth/commands/authorize.command'

import { HeaderUserKey } from '../constants/headeruser.key'
import { SessionKey } from '../constants/session.key'

@Injectable()
export class AuthorizeMiddleware implements NestMiddleware {
  constructor(
    @Inject(AUTH_SERVICE_NAME) private authServiceClient: ClientProxy,
  ) {}

  async use(req: Request, _res: Response, next: NextFunction): Promise<void> {
    const userId: string = get(req.headers, HeaderUserKey)

    let session: IAuthorizeCommandResponse
    try {
      session = await this.authServiceClient.send<IAuthorizeCommandResponse>(AUTHORIZE_COMMAND, userId).toPromise()
    } catch (error) {
      Logger.error('Unexpected occured while performing AUTHORIZE_COMMAND', JSON.stringify(error))
    }

    if (!session) {
      next(new UnauthorizedException())
      return
    }

    set(req, SessionKey, session)
    next()
  }
}
