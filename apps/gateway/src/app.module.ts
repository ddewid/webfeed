import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { AUTH_SERVICE_NAME, AUTH_QUEUE_NAME } from '@app/common/clients/auth/name'
import { AuthorizeMiddleware } from './middlewares/authorize.middleware'
import { FeedModule } from './feed/feed.module'

const QUEUE_URL = typeof process.env.QUEUE_URL === 'undefined' ? 'amqp://localhost:5672' : process.env.QUEUE_URL

@Module({
  imports: [
    ClientsModule.register([
      {
        name: AUTH_SERVICE_NAME,
        transport: Transport.RMQ,
        options: {
          urls: [QUEUE_URL],
          queue: AUTH_QUEUE_NAME,
          queueOptions: {
            durable: false
          },
        },
      }
    ]),
    FeedModule
  ]
})

export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(AuthorizeMiddleware)
      .forRoutes({ path: '*', method: RequestMethod.ALL })
  }
}
