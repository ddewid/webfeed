
import { Injectable, Inject } from '@nestjs/common'
import { ClientProxy } from '@nestjs/microservices'
import { UserFeedEntity } from '@app/common/entities/UserFeed'
import { UserFeedChunkEntity } from '@app/common/entities/UserFeedChunk'
import { FEED_SERVICE_NAME } from '@app/common/clients/feed/name'
import { GET_FEED_COMMAND, IGetFeedCommandResponse } from '@app/common/clients/feed/commands/get_feed.command'
import { GET_FEED_CHUNK_COMMAND, IGetFeedChunkCommandResponse } from '@app/common/clients/feed/commands/get_feed_chunk.command'


@Injectable()
export class FeedService {
  constructor(
    @Inject(FEED_SERVICE_NAME) private feedServiceClient: ClientProxy,
  ) {}

  async getFeed(userId: string): Promise<UserFeedEntity> {
    const response: IGetFeedCommandResponse = await this.feedServiceClient.send<IGetFeedCommandResponse>(
      GET_FEED_COMMAND,
      userId
    ).toPromise()

    return new UserFeedEntity({ ...response })
  }
  
  async getFeedChunk(chunkId: string): Promise<UserFeedChunkEntity> {
    const response: IGetFeedChunkCommandResponse = await this.feedServiceClient.send<IGetFeedChunkCommandResponse>(
      GET_FEED_CHUNK_COMMAND,
      chunkId
    ).toPromise()

    return new UserFeedChunkEntity({ ...response })
  }
}
