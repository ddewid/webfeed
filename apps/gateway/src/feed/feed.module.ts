import { Module } from '@nestjs/common'
import { ClientsModule, Transport } from '@nestjs/microservices'
import { FEED_SERVICE_NAME, FEED_QUEUE_NAME } from '@app/common/clients/feed/name'

import { FeedController } from './feed.controller'
import { FeedService } from './feed.service'

const QUEUE_URL = typeof process.env.QUEUE_URL === 'undefined' ? 'amqp://localhost:5672' : process.env.QUEUE_URL

@Module({
  imports: [
    ClientsModule.register([
      {
        name: FEED_SERVICE_NAME,
        transport: Transport.RMQ,
        options: {
          urls: [QUEUE_URL],
          queue: FEED_QUEUE_NAME,
          queueOptions: {
            durable: false
          },
        },
      },
    ]),
  ],
  controllers: [FeedController],
  providers: [FeedService],
})
export class FeedModule {}