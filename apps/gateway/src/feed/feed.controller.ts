import { Controller, Get, Req, Param, UseInterceptors, ClassSerializerInterceptor } from '@nestjs/common'
import { get } from 'lodash'
import { UserFeedEntity } from '@app/common/entities/UserFeed'
import { UserFeedChunkEntity } from '@app/common/entities/UserFeedChunk'

import { FeedService } from './feed.service'
import { SessionKey } from '../constants/session.key'
@Controller()
export class FeedController {
  constructor(
    private feedService: FeedService,
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get('/feed')
  async getFeed(@Req() req: Request): Promise<UserFeedEntity> {
    const session = get(req, SessionKey)

    const feed = await this.feedService.getFeed(session.userId)

    return feed
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get('/feed/chunk/:chunkId')
  async getFeedPostBatch(@Param('chunkId') chunkId: string): Promise<UserFeedChunkEntity> {
    const chunk = await this.feedService.getFeedChunk(chunkId)

    return chunk
  }
}
