import { NestFactory } from '@nestjs/core'
import { Logger } from '@nestjs/common'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import * as helmet from 'helmet'

import { ApplicationModule } from './app.module'

const PORT = 3000

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);

  const options = new DocumentBuilder()
    .setTitle('WebFeed example')
    .setDescription('The WebFeed API')
    .setVersion('1.0')
    .addTag('feed')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  app.enableCors()
  app.use(helmet())

  await app.listen(PORT).then(() => Logger.log(`Gateway is listening on PORT=${PORT}`))
}
bootstrap();
