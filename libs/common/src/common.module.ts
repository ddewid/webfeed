import { Module } from '@nestjs/common'
import { CommonService } from './common.service'

import { PostEntity } from './entities/Post'
import { SessionEntity } from './entities/Session'
import { UserEntity } from './entities/User'
import { UserFeedEntity } from './entities/UserFeed'
import { UserFeedChunkEntity } from './entities/UserFeedChunk'

import { EntityNotFound } from './errors/EntityNotFound'
import { ApplicationError } from './errors/ApplicationError'


const Entities: Array<any> = [PostEntity, SessionEntity, UserEntity, UserFeedEntity, UserFeedChunkEntity]
const Errors: Array<any> = [EntityNotFound, ApplicationError]

@Module({
  providers: [CommonService],
  exports: [
    CommonService,
    ...Entities,
    ...Errors
  ],
})
export class CommonModule {}
