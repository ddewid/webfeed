import { Exclude, Expose } from "class-transformer";


@Exclude()
export class UserFeedEntity {
  @Expose()
  feedId: string
  @Expose()
  userId: string

  @Expose({ name: 'chunkId' })
  chunkHeadHash: string
  
  constructor(partial: Partial<UserFeedEntity>) {
    Object.assign(this, partial);
  }
}
