export class PostEntity {
  content: string
  imageUrl: string
  tags: string[]
  likes: any
  comments: any
  createdBy: string
  createdAt: string
  updatedAt: string

  constructor(partial: Partial<PostEntity>) {
    Object.assign(this, partial);
  }
}
