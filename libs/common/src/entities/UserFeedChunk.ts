import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class UserFeedChunkEntity {
  @Expose()
  feedId: string

  chunkHash: string

  nextChunkHash: string

  @Expose()
  posts: any[]

  @Expose()
  visited: boolean

  @Expose()
  createdAt: string

  constructor(partial: Partial<UserFeedChunkEntity>) {
    Object.assign(this, partial);
  }
}
