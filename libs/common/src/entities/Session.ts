export class SessionEntity {
  sessionId: string
  userId: string
  expires: string
  revoked: boolean

  constructor(partial: Partial<SessionEntity>) {
    Object.assign(this, partial);
  }
}
