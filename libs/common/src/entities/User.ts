import { Exclude } from 'class-transformer';

export class UserEntity {
  userId: string
  name: string
  email: string

  @Exclude()
  passhash: string

  @Exclude()
  targetGroup: any

  createdAt: string
  updatedAt: string

  constructor(partial: Partial<UserEntity>) {
    Object.assign(this, partial);
  }
}
