export const ERROR_CODE = 'application_error'

export class ApplicationError extends Error {
  code = ERROR_CODE
}
