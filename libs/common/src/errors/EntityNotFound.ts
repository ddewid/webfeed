export const ERROR_CODE = 'not_found'

export class EntityNotFound extends Error {
  code = ERROR_CODE
}
