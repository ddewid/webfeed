export const GET_FEED_CHUNK_COMMAND = 'get_feed_chunk_command'

export interface IGetFeedChunkCommandResponse {
  feedId: string
  chunkHash: string
  nextChunkHash: string | null
  posts: any
  visited: boolean
  createdAt: string
}
