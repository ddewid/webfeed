export const GET_FEED_COMMAND = 'get_feed_command'

export interface IGetFeedCommandResponse {
  feedId: string
  userId: string
  chunkHeadHash: string | null
}
