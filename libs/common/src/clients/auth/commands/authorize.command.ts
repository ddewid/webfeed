export const AUTHORIZE_COMMAND = 'authorize_command'

// TODO @davlis
//  Extend entities
export interface IAuthorizeCommandResponse {
  sessionId: string
  userId: string
  expires: string
  revoked: boolean
}
