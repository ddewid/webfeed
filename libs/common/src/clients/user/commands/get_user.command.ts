export const GET_USER_COMMAND = 'get_user_comand'

export interface IGetUserCommandResponse {
  userId: string
  name: string
  email: string
  targetGroup: any
  createdAt: string
  updatedAt: string
}
