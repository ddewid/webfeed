# Existing Services
## Auth
This service is responsible for user authentication.

In future it would also be responsible for session initialization (login/renew). 

## User
Main purpose of this service is to handle all operations made to User entity.

In future it would also handle preferences, etc.

## Feed
This service is responsible for reading User Feeds and Feed Chunks (Posts).

In future if we would want to create FeedChunks this job would be delegated to another service "FeedDeamonWrite" and it would process operations in background.

## Gatway
Main purpose of this service is to handle HTTP calls from customers.

# Entities
## User
```javascript
//
// Note @davlis
//
@Schema()
export class User extends Document {
  @Prop({ index: true })
  userId: string

  @Prop({ index: true })
  email: string

  @Prop()
  name: string

  // Note: Related to preferences
  @Prop()
  targetGroup: any

  @Prop()
  createdAt: string

  @Prop()
  updatedAt: string
}
```

## Post
```javascript
//
// Note @davlis
//
//    User can create posts with text content and image.
//    In terms of {tags, likes, comments} our internal deamon services (workers) would generate/modify it in the background.
//
//    Image upload would be off-loaded to Image-service which would minimalize overhead with uploading using signed URLs.
@Schema()
export class Post extends Document {
  @Prop()
  content: string

  @Prop()
  imageUrl: string

  // TBD
  @Prop()
  tags: string[]

  // TBD
  @Prop()
  likes: any

  // TBD
  @Prop()
  comments: any

  @Prop({ index: true })
  createdBy: string

  @Prop()
  createdAt: string

  @Prop()
  updatedAt: string
}
```

## UserFeed
```javascript
//
// Note @davlis
//
//    User feed would be created automatically while user creation process.
//    At the very beggining chunkHeadHash should be set to nil,
//    that would mean for system that we gonna serve some super-common content for user / or that there is no posts.
// 
//    Once user initialization finished and when user preferences are set by our internal background processing workers,
//    our deamon service would create  "Post Chunk" List (reference: 'userfeedchunks') at given moment.
// 
//    Once new Post Chunk is avaiable then "UserFeed.chunkHeadHash" would be updated by internal deamon.
//
//    We can live with index and do not worry about latency while inserting
//    as these actions are going to be performed in background
@Schema()
export class UserFeed extends Document {
  @Prop({ index: true })
  feedId: string

  // Visibility purpose only
  @Prop()
  userId: string

  // uuid
  @Prop()
  chunkHeadHash: string
}
```

## UserFeedChunk
```javascript
//
// Note @davlis
//
//    This entity purpose is to serve newest posts within user feed without duplicates.
//    It's data is going to be created by our internal service deamons (workers) in background.
//
//    Our Internal deamon could create userfeedchunks maximum N "chunks" at given moment.
//  
//    Once GET /feed/:chunk is called system would do async call to change head of "userfeed" (side-effect)
//
//    When provided solution with chunks on database level would not meet proper performance to application scale
//    We could then try approach with FIFO queues with hashed keys
//    Database data would be persisted only for audit purposes.
@Schema()
export class UserFeedChunks extends Document {
  @Prop({ index: true })
  feedId: string

  @Prop({ index: true })
  chunkHash: string

  @Prop()
  nextChunkHash: string | null

  // Posts at given moment
  @Prop()
  posts: any[]

  @Prop()
  visited: boolean

  @Prop()
  createdAt: Date
}
```

# API Documentation
Api documentation is served by API gateway at `/api` route.

Just FYI: It has some unfixed bugs. Treat it as a reference only. In order to perform requests I highly recomment Postman or Curl.

Remember to provide `x-user` token that refers to existing user in User database.

# Notes for reviewers
I was in a rush a little bit with this application so not everything was covered.

Things that I'm aware that are, let's say, "MVP" state.

- Lack of unit/integration tests.
- Some values are hardcoded (e.g queue names), also there is none usage of ConfigService.
- Lack of seeder tool for database with some mock data.
- Lack of docker-compose with dev (local) environment integration in watch mode.
- Optimalization to workspace (monorepo) and container creation.
- Architecture diagrams (code/design/infrastructure)
- Lack of K8 cluster setup (I planned to:))
- Lack of stress tests (I planned to do it with Locust)
- Lack of deployment to cloud (GCP)
